use embedded_hal::blocking::spi::Transfer;
use embedded_hal::digital::v2::OutputPin;

use spi_memory::series25::Flash;
use spi_memory::Read;

use fatfs::{DefaultTimeProvider, FileSystem, FsOptions, LossyOemCpConverter, SeekFrom};

// structure to hold state for position within spi flash memory
pub struct FlashIO<SPI: Transfer<u8>, CS: OutputPin> {
    zero_position: u32,
    position: u32,
    flash: Flash<SPI, CS>,
}

// offset the beginning of the flash (for fatfs sake) by `lba` blocks
// each block is 512 bytes
impl<SPI: Transfer<u8>, CS: OutputPin> FlashIO<SPI, CS> {
    pub fn new(flash: Flash<SPI, CS>, lba: u32) -> Self {
        FlashIO {
            zero_position: lba * 512,
            position: lba * 512,
            flash,
        }
    }
}

// I don't properly know how to define my own error type that will work here
// TODO: use a custom error type, not `()`.
impl<SPI: Transfer<u8>, CS: OutputPin> fatfs::IoBase for FlashIO<SPI, CS> {
    type Error = ();
}

// impl Read
impl<SPI: Transfer<u8>, CS: OutputPin> fatfs::Read for FlashIO<SPI, CS> {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, Self::Error> {
        match self.flash.read(self.position, buf) {
            Ok(_) => {
                self.position += buf.len() as u32;
                Ok(buf.len())
            }
            Err(_) => Err(()),
        }
    }
}

// impl Write
// TODO: actually implement this
impl<SPI: Transfer<u8>, CS: OutputPin> fatfs::Write for FlashIO<SPI, CS> {
    fn write(&mut self, _buf: &[u8]) -> Result<usize, Self::Error> {
        Ok(0)
    }

    fn flush(&mut self) -> Result<(), Self::Error> {
        Ok(())
    }
}

// impl Seek
impl<SPI: Transfer<u8>, CS: OutputPin> fatfs::Seek for FlashIO<SPI, CS> {
    fn seek(&mut self, pos: SeekFrom) -> Result<u64, Self::Error> {
        match pos {
            SeekFrom::Start(offset) => self.position = (self.zero_position as u64 + offset) as u32,
            SeekFrom::End(inset) => {
                self.position = (self.zero_position as i64 + 512 * 1024 - inset) as u32
            }
            SeekFrom::Current(offset) if offset < 0 => self.position -= (-offset) as u32,
            SeekFrom::Current(offset) => self.position += offset as u32,
        }
        Ok(self.position.into())
    }
}

// create a fatfs filesystem around our flash_io driver
pub fn filesystem<SPI: Transfer<u8>, CS: OutputPin>(
    flash_io: FlashIO<SPI, CS>,
) -> FileSystem<FlashIO<SPI, CS>, DefaultTimeProvider, LossyOemCpConverter> {
    FileSystem::new(flash_io, FsOptions::new()).unwrap()
}
