.PHONY: flash
flash: firmware.bin
	dfu-util -D firmware.bin -d "0483:df11" -a 0 -s 0x08004000

uf2: firmware.bin
	uf2conv firmware.bin --base=0x08004000 --output firmware.uf2

.PHONY: build
build:
	(cargo build --release && cargo objcopy --release -- -O binary firmware.bin)

.PHONY: serial
serial:
	screen /dev/ttyACM0 9600

firmware.bin: build

.PHONY: clean
clean:
	cargo clean
