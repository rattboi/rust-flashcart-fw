#![no_main]
#![no_std]
#![feature(alloc_error_handler)]

extern crate alloc;

use alloc::vec::Vec;
use alloc_cortex_m::CortexMHeap;
use core::alloc::Layout;

#[global_allocator]
static ALLOCATOR: CortexMHeap = CortexMHeap::empty();

// used for entrypoint, since `no_main`
use cortex_m_rt::entry;

// stm32f4xx hal crate
use stm32f4xx_hal as hal;

use embedded_hal::{digital::v2::OutputPin, spi::MODE_0};
use hal::{prelude::*, serial::config::Config, serial::Serial, spi::Spi, stm32};

// used to echo panic response over serial port
use panic_write::PanicHandler;

// for pretty formatting of the serial output
use core::fmt::Write;

use firmware::{handle_event, init_flash_and_fatfs, run_menu, EmuData};

mod error;

#[entry]
fn entry() -> ! {
    // Initialize the allocator BEFORE you use it
    let start = cortex_m_rt::heap_start() as usize;
    let size = 1024; // in bytes
    unsafe { ALLOCATOR.init(start, size) }

    let mut xs = Vec::new();
    xs.push(1);

    loop {
        // trampoline to `main`, where we can use Result<T,E> and `?` operator
        if main().is_ok() {}
    }
}

fn main() -> Result<(), firmware::error::Error> {
    let dp = stm32::Peripherals::take().expect("failed to get stm32 peripherals");

    // Take ownership over the raw devices and convert them into the corresponding
    // HAL structs.
    // RCC = Reset and Clock Control
    let rcc = dp.RCC.constrain();

    // The various system clocks need to be configured to particular values
    // to work with USB - we'll set them up here.
    //
    // the use of `unsafe` is required here by the stm32 hal to overclock
    // the stm32f411 is supposed to run at 100MHz max, but can safely run at 120MHz
    let clocks = unsafe {
        rcc.cfgr
            .use_hse(8.mhz()) // Use the High Speed External 8MHz crystal
            .sysclk(120.mhz()) // The main system clock will be 120MHz (overclocked)
            .require_pll48clk() // require the internal PLL to be 48MHz, for USB to function properly
            .freeze_unchecked() // use `freeze_unchecked` because of the overclock (requires `unsafe`)
    };

    // Grab the GPIO banks we'll use.
    let gpioa = dp.GPIOA.split();
    let gpiob = dp.GPIOB.split();
    let gpioc = dp.GPIOC.split();

    // Set up the LED (B0).
    let mut led = gpiob.pb0.into_push_pull_output();

    // Set up serial port (RXD/TXD) (B6/B7)
    let serial_txd = gpiob.pb6.into_alternate_af7();
    let serial_rxd = gpiob.pb7.into_alternate_af7();

    let serial = Serial::usart1(
        dp.USART1,
        (serial_txd, serial_rxd),
        Config::default().baudrate(115_200.bps()),
        clocks,
    )?;

    // grab tx and rx (but we never use rx)
    let (tx, mut _rx) = serial.split();

    // set up the panic handler
    let mut tx = PanicHandler::new(tx);

    // set up SPI/flash memory
    let cs = {
        let mut cs = gpioa.pa15.into_push_pull_output();
        cs.set_high()?; // deselect
        cs
    };

    let spi = {
        let sck = gpiob.pb3.into_alternate_af5();
        let miso = gpiob.pb4.into_alternate_af5();
        let mosi = gpiob.pb5.into_alternate_af5();

        Spi::spi1(dp.SPI1, (sck, miso, mosi), MODE_0, 1.mhz().into(), clocks)
    };

    // initialize the spi flash and use fatfs
    // TODO: return the fatfs reference from this in order to
    //   use it outside this routine
    if crate::init_flash_and_fatfs(spi, cs, &mut tx).is_err() {
        writeln!(tx, "Error in flash/fatfs initialization")?;
    };

    // initialize the on-board LED to "off"
    led.set_low()?;

    // initialize vectrex-interfacing pins
    init_gpioa(
        gpioa.pa0, gpioa.pa1, gpioa.pa2, gpioa.pa3, gpioa.pa4, gpioa.pa5, gpioa.pa6, gpioa.pa7,
    );

    init_gpiob(gpiob.pb1, gpiob.pb10, gpiob.pb15);

    init_gpioc(
        gpioc.pc0, gpioc.pc1, gpioc.pc2, gpioc.pc3, gpioc.pc4, gpioc.pc5, gpioc.pc6, gpioc.pc7,
        gpioc.pc8, gpioc.pc9, gpioc.pc10, gpioc.pc11, gpioc.pc12, gpioc.pc13, gpioc.pc14,
        gpioc.pc15,
    );

    run_menu(&mut tx, rom_emu)
}

#[alloc_error_handler]
fn oom(_: Layout) -> ! {
    loop {}
}

// "safe" wrapper around volatile adddresses
use voladdress::{Safe, VolAddress};

// for initializing our address/control/data lines
use stm32f4xx_hal::gpio::{gpioa, gpiob, gpioc, Floating, Input};

pub fn rom_emu(emu_data: &mut EmuData) {
    // GPIOA -> Input or Output?
    const GPIOA_IO_STATE: VolAddress<u32, Safe, Safe> = unsafe { VolAddress::new(0x4002_0000) };
    // Read from GPIOA (all at once, as a bus)
    const GPIOA_INPUT_READ: VolAddress<u32, Safe, ()> = unsafe { VolAddress::new(0x4002_0010) };
    // Write to GPIO (all at once, as a bus)
    const GPIOA_OUTPUT_WRITE: VolAddress<u32, (), Safe> = unsafe { VolAddress::new(0x4002_0018) };
    // Read from GPIOB (control lines)
    const GPIOB_INPUT_READ: VolAddress<u32, Safe, ()> = unsafe { VolAddress::new(0x4002_0410) };
    // Read from GPIOC (address lines)
    const GPIOC_INPUT_READ: VolAddress<u32, Safe, ()> = unsafe { VolAddress::new(0x4002_0810) };

    loop {
        // wait for nCART to become active
        while GPIOB_INPUT_READ.read() & (1 << 15) != 0 {}

        // read or write?
        match GPIOB_INPUT_READ.read() & (1 << 1) == (1 << 1) {
            // deal with write
            false => {
                // save the GPIO in/out state. We're going to make the data bus inputs for a sec
                let original_data_bus_state = GPIOA_IO_STATE.read();
                // mask off lower 16 bits and write back
                GPIOA_IO_STATE.write(original_data_bus_state & 0xFFFF0000u32);

                // get the current address on the bus
                let cart_addr = GPIOC_INPUT_READ.read() & 0x7FFFu32;

                // wait for /E low, (data valid)
                while GPIOB_INPUT_READ.read() & (1 << 10) != 0 {}

                // get the data being written from the vectrex
                let cart_data = (GPIOA_INPUT_READ.read() & 0xFFu32) as u8;

                // restore data bus to original state (data bus as outputs)
                GPIOA_IO_STATE.write(original_data_bus_state);

                // write to address 0 => write data bus byte to serial port
                if cart_addr == 0 {
                    // this is too slow, but we're not really using it so that's ok
                    // TODO: use itm logger instead (requires potential hardware revision)
                    //   itm is faster than usart (2mbps vs 512kbps max), and async, so we can dump and go
                    writeln!(emu_data.tx, "{}\r", cart_data).unwrap();
                }

                let write_cart_type = (cart_addr & 0xFFu32) as u8;
                match write_cart_type {
                    0xFF => {
                        // rpc call
                        crate::handle_event(cart_data, emu_data)
                    }
                    _ => {
                        //// parameter write
                        // parameters are how RPC calls can give arguments
                        // 1) do the param writes first
                        // 2) call RPC
                        // 3) rpc code looks at params for arguments
                        emu_data.parm_ram[write_cart_type as usize] = cart_data;

                        // wait for nWRITE to be unasserted
                        while (GPIOB_INPUT_READ.read() & (1 << 1)) == 0 {}
                    }
                }
            }

            // deal with read
            true => {
                // Read address from bus, invert top bit
                // MSB of address is not an actual address line, but is used by vectrex software
                //   for bankswitching. It is active low.
                let addr = GPIOC_INPUT_READ.read() ^ 0x8000u32;

                // make sure that the address is actually meant for the cart,
                //   and that we don't out-of-bounds index into our cart_data
                if (addr as usize) < emu_data.cart_data.len() {
                    // output writing is weird
                    //   each GPIO port is 16-bit, GPIO{0-15}, the top 16 bits are RESET bits,
                    //   while the bottom 16 bits are SET bits.
                    // ORing 0x00FF0000 with our data is saying:
                    //   reset all the bits from GPIO{7-0}, and set the bits
                    //   given in `d`. SET takes precedence.
                    // This sort of goofy interface is why `unsafe` seemed necessary
                    // Treating each pin as an individual separately-owned object
                    //  doesn't make sense for wider parallel busses where you want to
                    //  make sure that the MMIO register is set all a the same time.
                    let d = (emu_data.cart_data[addr as usize] as u32) | 0xFF0000u32;
                    // Write our weird u32 with SET and RESET bits to the GPIOA output register
                    GPIOA_OUTPUT_WRITE.write(d)
                }
            }
        }
    }
}

// I disabled clippy here because I wasn't able to figure out a more generic way to do this
// The rust typeclasses in hal/embedded-hal, along with the `split()` partial borrowing
//   seemingly make this hard to generalize over. Or maybe I just don't know how to do it.
//
// perhaps just wrapping all of these in a tuple would have been ok, since it *is* a bus
// I *think* that's what embedded-hal does for talking about the SPI bus
#[allow(clippy::too_many_arguments)]
pub fn init_gpioa(
    pa0: gpioa::PA0<Input<Floating>>,
    pa1: gpioa::PA1<Input<Floating>>,
    pa2: gpioa::PA2<Input<Floating>>,
    pa3: gpioa::PA3<Input<Floating>>,
    pa4: gpioa::PA4<Input<Floating>>,
    pa5: gpioa::PA5<Input<Floating>>,
    pa6: gpioa::PA6<Input<Floating>>,
    pa7: gpioa::PA7<Input<Floating>>,
) {
    // data bus (output, mostly)
    pa0.into_push_pull_output();
    pa1.into_push_pull_output();
    pa2.into_push_pull_output();
    pa3.into_push_pull_output();
    pa4.into_push_pull_output();
    pa5.into_push_pull_output();
    pa6.into_push_pull_output();
    pa7.into_push_pull_output();
}

pub fn init_gpiob(
    pb1: gpiob::PB1<Input<Floating>>,
    pb10: gpiob::PB10<Input<Floating>>,
    pb15: gpiob::PB15<Input<Floating>>,
) {
    // control lines (input)
    pb1.into_pull_down_input(); //  READ/nWRITE
    pb10.into_pull_down_input(); // nENABLE
    pb15.into_pull_down_input(); // nCART_ACCESS
}

#[allow(clippy::too_many_arguments)]
pub fn init_gpioc(
    pc0: gpioc::PC0<Input<Floating>>,
    pc1: gpioc::PC1<Input<Floating>>,
    pc2: gpioc::PC2<Input<Floating>>,
    pc3: gpioc::PC3<Input<Floating>>,
    pc4: gpioc::PC4<Input<Floating>>,
    pc5: gpioc::PC5<Input<Floating>>,
    pc6: gpioc::PC6<Input<Floating>>,
    pc7: gpioc::PC7<Input<Floating>>,
    pc8: gpioc::PC8<Input<Floating>>,
    pc9: gpioc::PC9<Input<Floating>>,
    pc10: gpioc::PC10<Input<Floating>>,
    pc11: gpioc::PC11<Input<Floating>>,
    pc12: gpioc::PC12<Input<Floating>>,
    pc13: gpioc::PC13<Input<Floating>>,
    pc14: gpioc::PC14<Input<Floating>>,
    pc15: gpioc::PC15<Input<Floating>>,
) {
    // address bus (input)
    pc0.into_pull_down_input();
    pc1.into_pull_down_input();
    pc2.into_pull_down_input();
    pc3.into_pull_down_input();
    pc4.into_pull_down_input();
    pc5.into_pull_down_input();
    pc6.into_pull_down_input();
    pc7.into_pull_down_input();
    pc8.into_pull_down_input();
    pc9.into_pull_down_input();
    pc10.into_pull_down_input();
    pc11.into_pull_down_input();
    pc12.into_pull_down_input();
    pc13.into_pull_down_input();
    pc14.into_pull_down_input();
    pc15.into_pull_down_input();
}
