# rust-flashcart-fw

### Bradon Kanyid

### Description

This is an alternative firmware, written in Rust, for the [VEXTREME](https://github.com/technobly/VEXTREME), a flashcart for the [Vectrex](https://en.wikipedia.org/wiki/Vectrex).

[Youtube Demo](https://www.youtube.com/watch?v=Nc39w5gAdrQ)

The VEXTREME firmware has thus far been developed in C/ARM assembly. I wanted to try rewriting the firmware in Rust, and attempting to make better interfaces/abstraction that will allow extracting the generic components of the firmware from the platform-specific portions. This will allow development of the flashcart "core" as a library, and then develop many separate platform-specific implementations leveraging the core library.

### Important Legal Note

There are two Vectrex games checked into the repository that I used for testing, and are actually included and embedded in the firmware on build. This would generally be illegal to distribute someone's copyrighted games, but actually GCE has made their Vectrex games free-to-distribute.

### Features

The core functionality of the flashcart firmware is to act as a "rom emulator". When the Vectrex CPU asks for data on its address lines, the firmware supplies data for that address. Doing this with a microcontroller means "racing the beam", i.e. you need to get the data out on the bus quickly after the address lines are set. If you don't make it, the Vectrex will see the wrong data, and will not run correctly. This means we have to be **fast**.

Another core feature of the flashcart firmware is to supply various RPC functions that the Vectrex can request for the firmware to do. Many of these are used by the Vectrex menu that is embedded in the firmware. Examples of RPC commands are things like:

* Load File Listing
* Save Settings
* Start ROM
* Enter Dev Mode
* Change Cart LED colors

### Theory of Operation

On Vectrex startup, the firmware presents the Vectrex with the menu, which is embedded into it. The menu uses the RPC mechanism to request various actions from the firmware. It initially loads a list of the files on the 16MB SPI flash chip, presents them to the user, and when one is selected, the firmware loads said file into RAM and presents that new data to the Vectrex CPU via the rom emulator.

##### Vectrex to Firmware Communication

The RPC mechanism is how the Vectrex communicates commands to the firmware. If the Vectrex attempts to write to cartridge address space, that is a signal that it is intended as an RPC, as a "real game" would not try to write to ROM. If the write is to a cart address ending in FF, that triggers the RPC call. The data written on the data bus is the RPC number, which represents which command to call. If the write is to a cart address whose low-order byte is not FF, that is a write to parameter ram. These parameters are used as arguments when the RPC call is made.

Example:
```
// from the Vectrex CPU perspective
STORE #01,$FFFE      // write #01 to parameter_ram[0xFE]
STORE #02,$FFFD      // write #02 to parameter_ram[0xFD]
STORE #05,$FFFF      // call RPC #5 (which would use parameter_ram[0xFD-0xFE]
```

The length of time it takes to run the RPC command is unknown but can be long. Because of this, the firmware cannot concurrently act as a "rom emulator" during this period, so the Vectrex CPU must call the RPC command from a subroutine running in Vectrex RAM space. The Vectrex can detect that "rom emulation" is resumed by looking for the Vectrex ROM header in the first few bytes of cart space. Once it has detected "rom emulation" has resumed, it can jump back into code in the rom space and continue what it was doing.

##### Firmware to Vectrex Communication

Because there is no straight-forward control of the Vectrex from the firmware side (injecting instructions or temporarily overriding flow control are both non-starters because of how sensitive the timing of Vectrex programs are), the only practical way to move data from the firmware to the Vectrex is by rewriting the rom it is emulating. Thus the virtual rom should be mutable, using a sort of "shared memory" section to quickly move chunks of data from the firmware into a space that the Vectrex can read it.

### Building

Building the project is straightforward.

First steps are to set up a cross-compiling environment. Do the following steps:

```
rustup default stable
rustup target add thumbv7em-none-eabihf
rustup component add llvm-tools-preview
cargo install cargo-binutils
```

There's a `Makefile` in the root of the project that does the required steps to flash the VEXTREME.

```
cargo build --release
cargo objcopy --release -- -O binary firmware.bin
dfu-util -D firmware.bin -d "0483:df11" -a 0 -s 0x08000000
```

### Testing

There are no tests, unfortunately. Most of the code I wrote does not lend itself to unit-testing, per se. I was doing frequent on-hardware tests. I can imagine in the future, once the generic portion is fully extracted into its own library, that you could use QEMU and mock out the various external peripherals that are interfaced. I did not get that far.

I don't feel confident that if I had gone down the testing route that I would have been able to test the most important things, which were very timing-sensitive. I initially ported over the rom emulator code as inline assembly, using the nightly `asm!` macro feature. I decided to try to move it to native rust code, and had a lot of difficulty with meeting timing requirements. This would not have been capturable in a unit test suite.

I did a *lot* of manual testing. Especially in the beginning as I was bootstrapping various components, it required loading the code onto the board and testing it myself to verify. I played a **lot** of Scramble, which is a great game, so I'm ok with it.

### What Went Well

Bootstrapping the board initially actually was more straightforward than I expected. I searched for larger embedded rust projects and came across this article: [Using Rust for a simple hardware project](https://blog.tonari.no/rust-simple-hardware-project). The repository linked to the article is [here](https://github.com/tonarino/panel-firmware), and I leveraged the code there quite a bit for the initial bootstrapping.

After bootstrapping, I had to get the rom emulator running. As I already mentioned, I intially direct-ported the inline assembly from the C/assembly project. I was having a hard time getting the distinct interface I wanted between the rust code and the assembly, so I made the decision to rewrite it directly in rust. I ran into various problems, some of which I'm concerned may come back as I continue to develop it, because I can't trust the compiler to not rearrange things that are logically equivalent, but functionally different. Overall, it was more straightforward than expected. I did need to use `unsafe` in order to make sure that my parallel bus reads and writes happened simultaneously, but I think that's acceptable. 

The ecosystem of embedded rust crates was much better than expected. I was able to get a lot of functionality without writing it all from scratch. I'm very excited about the future of embedded rust.

### What Didn't Go Well

It seems like there's a few impedence mismatches, or maybe things that I don't understand in Rust yet. The stm32 HAL library gives ownership to individual pins, but doing this makes it very hard to do things like wide parallel busses. The Vectrex address bus is 16 bits wide. I don't believe writing across the individual pins would amount to a single MMIO register write like it needs to.

Also, any attempt at extracting functions from the main code was a chore if it involved any partially-owned pins. You can see in `init_gpioc`, I was reduced to passing in 16 separate pins as 16 separate arguments to avoid the wraith of the borrow checker. The traits and typeclasses that enforce the various state transitions on pins are also extremely ugly to use if passed from function to function. I feel I must just misunderstand something here, because I'm sure it shouldn't be as difficult as it seemed.

I had some interesting problems when I wanted to get fatfs working. It requires supplying trait impls for `std::io::read/write/seek` for the library to use for your media. The problem is that `[nostd]` doesn't have `std::io`. The initial solution was to use a very janky crate called [`core_io`](https://crates.io/crates/core_io). This requires you to use a *specific* version of nightly because they used some sort of automation to convert `std::io` to `no_std::io`. I got this working, but was very unhappy being pinned to an out-of-date version of `nightly`. I did further research, and found that fatfs 0.4.0 is unreleased, but replaces the `io::` traits with its own implementations, which meant no more dependency on `core_io`. I am using a git checkout version of that dependency now. I should probably pin that to a specific SHA, but that should be captured in the `Cargo.lock` file.

I also had one instance where using `unsafe` to quickly read from an array without out-of-bounds panicking, came back and bit me. It worked great for reading a virtual game from stm32 flash rom space, but was triggering something unknown when using the same from stm32 ram. My theory is that the MPU didn't like that I was reading some section after the RAM, and it was triggering a trap. I ended up replacing the `unsafe` code with my own bounds-checking, and the bug went away.

```rust
-     let d = unsafe { (*data.get_unchecked(addr as usize) as u32) | 0xFF0000u32 };
+     if (addr as usize) < emu_data.cart_data.len() {
+                let d = (emu_data.cart_data[addr as usize] as u32) | 0xFF0000u32;
```

### Future Plans

I definitely plan to continue working on this project. 

* A lot of RPC functionality isn't implemented. 
* Continue with the FAT32 reading integration into the main code. 
* I basically have the project inverted from what I want. The generic code contains the entrypoint, and the vectrex-specific code is in a separate module that acts as a library. I need to separate out the vectrex code entirely into another project, which then can use the generic rust firmware as a library. That will further enforce the interface I'm trying to define.
* Encapsulate the various shared mutable state that is given to the rom emulator into a more structured format than `&[u8]`.
* Get usb mass storage support added back. There's a library for it already, but I didn't have the primitives in place to implement it.

### License

This code is licensed GPL3. You can find [a copy in this repository](./LICENSE)
