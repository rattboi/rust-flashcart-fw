#![no_std]

// Used for `from_primitive`
extern crate num;
#[macro_use]
extern crate num_derive;

// stm32f4xx hal crate
use stm32f4xx_hal as hal;

// embedded-hal gives many of the functions we need for interacting
// with the stm32f411 in a generic way. Would make it easy(er) to move
// this code to a different chip
use hal::{pac::USART1, serial::Tx, spi::Spi, stm32};

// used to interact with the spi flash memory chip on the board
use spi_memory::series25::Flash;
use spi_memory::Read;

// for pretty formatting of the serial output
use core::fmt::Write;

// for reading MBR block from spi flash
use mbr_nostd::{self, MasterBootRecord, PartitionTable, PartitionType::Fat32};

// adapter for using spi-flash with fatfs
use crate::flash_io::FlashIO;
pub mod error;
pub mod flash_io;

// static resources embedded into firmware
// TODO: remove and load dynamically at runtime from spi-flash
// this requires full fatfs functionality
static MENU: &[u8] = include_bytes!("menu.bin");
static SCRAMBLE_DATA: &[u8] = include_bytes!("scramble.vec");
static ARMOR_DATA: &[u8] = include_bytes!("armor.vec");

// this struct is used to pass various bits of state to the rom emulator
// this is only slightly better than using mutable global state, but at
// least it's constrained to this small block
// TODO: formalize the parm_ram and cart_data members to be more structured
// i.e not just chunks of u8 memory
pub struct EmuData<'a> {
    pub parm_ram: [u8; 256],
    pub cart_data: &'a mut [u8],
    pub tx: &'a mut Tx<USART1>,
}

pub fn run_menu(tx: &mut Tx<USART1>, rom_emu: fn(&mut EmuData) -> ()) -> Result<(), error::Error> {
    writeln!(tx, "Loading Menu\r")?;
    let mut menu_data = [0; (20 * 1024)];
    menu_data[..MENU.len()].copy_from_slice(MENU);

    let mut emu_data = EmuData {
        parm_ram: [0; 256],
        cart_data: &mut menu_data,
        tx,
    };
    rom_emu(&mut emu_data);
    Ok(())
}

// I haven't figured out how to make this more simple yet.
// There's no reason that it needs to be this concrete
// (i.e. it should work across the set of pins that are in Alternate Function 5)
// there exists another set on GPIOA that would work
type FlashSpi = Spi<
    stm32::SPI1,
    (
        hal::gpio::gpiob::PB3<hal::gpio::Alternate<hal::gpio::AF5>>,
        hal::gpio::gpiob::PB4<hal::gpio::Alternate<hal::gpio::AF5>>,
        hal::gpio::gpiob::PB5<hal::gpio::Alternate<hal::gpio::AF5>>,
    ),
>;

// same goes for this
// there's nothing about FlashCs that really requires PA15
// although it is technically the correct pin for our board
// refactoring the hardware design would require changing this type,
// and that seems strange to me
type FlashCs = hal::gpio::gpioa::PA15<hal::gpio::Output<hal::gpio::PushPull>>;

pub fn init_flash_and_fatfs(
    spi: FlashSpi,
    cs: FlashCs,
    tx: &mut Tx<USART1>,
) -> Result<(), error::Error> {
    // init spi-flash and read jedec id
    let mut flash = Flash::init(spi, cs)?;
    let id = flash.read_jedec_id()?;
    writeln!(tx, "{:?}\r", id)?;

    // read first block of flash chip for MBR
    let mut mbr_buf = [0; 512];
    flash.read(0, &mut mbr_buf)?;
    let mbr = MasterBootRecord::from_bytes(&mbr_buf)?;

    // find first FAT32 partition's logical block from the MBR
    // defaults to 0 if it can't find one. Would probably want to error in a better way in reality
    // TODO: handle no FAT32 partitions better
    let fat_lba = mbr
        .partition_table_entries()
        .iter()
        .find(|p| matches!(p.partition_type, Fat32(_)))
        .map(|p| p.logical_block_address)
        .unwrap_or_default();

    // create flash-io adapter for fatfs to use to access flash
    // implements read/write/seek traits used by fatfs
    let fio = FlashIO::new(flash, fat_lba);

    // get fatfs from flash
    let fs = flash_io::filesystem(fio);

    // test code: open root directory, then navigate to /ROMS/1A-ZWO~1, where the roms are
    let games_dir = fs.root_dir().open_dir("ROMS")?.open_dir("1A-ZWO~1")?;

    // test code: list files in /ROMS/1A-ZWO~1
    games_dir.iter().for_each(|entry| {
        writeln!(tx, "{}\r", entry.unwrap().file_name()).unwrap();
    });

    Ok(())
}

// This enum defines the generic RPC commands used by the cart firmware
// these should be generic across platforms. It is conceivable that there would be
// platform-specific RPC commands as well, so perhaps a range of these would be left
// for the platform-specific side. The impetus would be to make as many of these generic as possible,
// to allow the generic firmware to implement it in one place and the specific platforms would all benefit
#[derive(Debug, FromPrimitive)]
enum RpcCommand {
    ChangeRom = 1,
    LoadStreamData = 2,
    ParentDir = 3,
    LedUpdateAll = 4,
    LedRainbowStep = 5,
    LedUpdateOne = 6,
    LedUpdateMulti = 7,
    LetSetBrightnessAll = 8,
    LoadVersions = 9,
    RamDisk = 10,
    LoadApp = 11,
    LoadSysOpts = 12,
    DumpMemory = 13,
    HighscoreSave = 14,
    StartRom = 15,
    LoadParmRam = 16,
    StoreToRom = 17,
    SettingsSave = 18,
}

//// generic RPC handler
// When the rom signals via RPC mechanism (per-device, defined in rom_emu)
// that it wants the cart to do something, jump here to handle it
// `rpc_code` is converted from u8 to the RpcCommand enum via `derive_primitive`
pub fn handle_event(rpc_code: u8, emu_data: &mut EmuData) {
    let rpc = num::FromPrimitive::from_u8(rpc_code);
    match rpc {
        Some(RpcCommand::StartRom) => start_rom(emu_data),
        Some(RpcCommand::StoreToRom) => store_to_rom(emu_data),
        Some(RpcCommand::LedRainbowStep) => {} // ignore this one, it's too noisy
        Some(other_rpc) => {
            writeln!(emu_data.tx, "Handling RPC call: {:?}\r", other_rpc).unwrap();
        }
        None => {}
    }
}

//// the most important RPC command
// this takes an index from parm_ram (set by the rom before calling RPC)
// and uses that index to load a new game
// when the RPC returns, the caller resets to address 0 to start the newly loaded game
fn start_rom(emu_data: &mut EmuData) {
    let arg = emu_data.parm_ram[254] as usize;
    writeln!(emu_data.tx, "Handling StartRom call: arg = {}\r", arg).unwrap();
    let rom_list = [SCRAMBLE_DATA, ARMOR_DATA];
    if arg < rom_list.len() {
        let rom = rom_list[arg];
        // overlay the static rom data into the ram buffer (20K)
        emu_data.cart_data[..rom.len()].copy_from_slice(rom);
    }
}

//// another RPC command
// this one allows a rom to write to itself
// this is used by the menu to store settings
// TODO: potentially save settings to fatfs and load on start
fn store_to_rom(emu_data: &mut EmuData) {
    let parm_ram = emu_data.parm_ram;
    let addr = ((parm_ram[0xF0u8 as usize] as u16) << 8) + parm_ram[0xF1u8 as usize] as u16;
    let value = parm_ram[0xF2];
    emu_data.cart_data[addr as usize] = value;
}
