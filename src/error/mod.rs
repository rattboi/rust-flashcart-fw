use hal::{
    gpio::{gpioa, gpiob, Alternate, Output, PushPull, AF5},
    serial::config,
};
use mbr_nostd::MbrError;
use stm32f4xx_hal as hal;

// this is really a stub, but allows us to use `main() -> Result<(),error::Error>`
pub enum Error {}

// these are completely mechanical to make the typechecker happy
// core errors
impl From<core::convert::Infallible> for Error {
    fn from(_: core::convert::Infallible) -> Self {
        todo!()
    }
}

impl From<core::fmt::Error> for Error {
    fn from(_: core::fmt::Error) -> Self {
        todo!()
    }
}

// hal errors
impl From<config::InvalidConfig> for Error {
    fn from(_: config::InvalidConfig) -> Self {
        todo!()
    }
}

// I separated this one out to a huge type alias, for obvious reasons
type SpiFlash = spi_memory::Error<
    hal::spi::Spi<
        hal::stm32::SPI1,
        (
            gpiob::PB3<Alternate<AF5>>,
            gpiob::PB4<Alternate<AF5>>,
            gpiob::PB5<Alternate<AF5>>,
        ),
    >,
    gpioa::PA15<Output<PushPull>>,
>;

impl From<SpiFlash> for Error {
    fn from(_: SpiFlash) -> Self {
        todo!()
    }
}

// mbr errors
impl From<MbrError> for Error {
    fn from(_: MbrError) -> Self {
        todo!()
    }
}

// fatfs errors
impl From<fatfs::Error<()>> for Error {
    fn from(_: fatfs::Error<()>) -> Self {
        todo!()
    }
}
